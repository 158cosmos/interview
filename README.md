打造最全、解析最清晰的 Java 系列面试题题，持续更新中...

<p align="center">
<a href="https://github.com/vipstone/interview" target="_blank">
	<img height="250" src="img/readme-1.png" width="250"/>
</a>
</p>


<hr>

## 2022 年更新内容...

<a href="https://mp.weixin.qq.com/s/i-EzrUVJon3ey3HZMqLWdw" target="_blank">1.int和Integer有什么区别？为什么要有包装类？</a>

<a href="https://mp.weixin.qq.com/s/eR0LmlAXb2w0qe3nzVJnsw" target="_blank">2.说一下final关键字和final的4种用法？</a>

<a href="https://mp.weixin.qq.com/s/U3FdjfEWICDPSjgy_WEJSA" target="_blank">3.final、finally、finalize 有什么区别？</a>

<a href="https://mp.weixin.qq.com/s/40zaEJEkQYM3Awk2EwIrWA" target="_blank">4.重写 equals 时为什么一定要重写 hashCode？</a>

<a href="https://mp.weixin.qq.com/s/Dnkx_uwhZqxipcpGPVVtvg" target="_blank">5.HashMap有几种遍历方法？推荐使用哪种？</a>

<a href="https://mp.weixin.qq.com/s/X8OW7Cpy1Bu2IJJp52BMcg" target="_blank">6.元素排序Comparable和Comparator有什么区别？</a>

<a href="https://mp.weixin.qq.com/s/m-zT_7R8WAvoty7rjSYQAQ" target="_blank">7.如何实现 List 集合去重？</a>

<a href="https://mp.weixin.qq.com/s/ASknNKns4nDPGhWxqaEEvA" target="_blank">8.HashSet如何保证元素不重复？</a>

<a href="https://mp.weixin.qq.com/s/kF1F4YgjcxmfAccgAk6m5g" target="_blank">9.接口和抽象类有什么区别？</a>

<a href="https://mp.weixin.qq.com/s/-eXL-Y6DHC_dX65PNQTq6w" target="_blank">10.this和super有什么区别？this能调用到父类吗？</a>

<a href="https://mp.weixin.qq.com/s/_I72minTniH5SAtsDaajYA" target="_blank">11.方法重写时需要注意哪些问题？</a>

<a href="https://mp.weixin.qq.com/s/4pi1OZx8So6GjHD6yxjB3Q" target="_blank">12.为什么不同返回类型不算方法重载？</a>

<a href="https://mp.weixin.qq.com/s/foM_-ozFVjPQ9qn22XnYeA" target="_blank">13.方法优先调用可选参数还是固定参数？</a>

<a href="https://mp.weixin.qq.com/s/6tTF-PPL07no42XXXTBn9w" target="_blank">14：方法重写和方法重载有什么区别？</a>

<a href="https://mp.weixin.qq.com/s/xX8QdojXdm36yvS5QUy3nA" target="_blank">15：说一下HashMap底层实现？及元素添加流程？</a>

<a href="https://mp.weixin.qq.com/s/rOTv9RDsHJxLWlJuPIdB8g" target="_blank">16：为什么HashMap会产生死循环？</a>

<a href="https://mp.weixin.qq.com/s/RfZEYrkDE1gBoP9VY7FbnA" target="_blank">17：HashMap除了死循环之外，还有什么问题？</a>

<a href="https://mp.weixin.qq.com/s/uibfTQUPXkdyx6lQoNtyQQ" target="_blank">18：为什么ConcurrentHashMap是线程安全的？</a>

<a href="https://mp.weixin.qq.com/s/T0h8-wnnkIRoa5PCtvHA_g" target="_blank">19：为什么ConcurrentHashMap不允许插入null值？</a>

<a href="https://mp.weixin.qq.com/s/PHYA51CgPYzg5_o2bKMZ4A" target="_blank">20：进程和线程有什么区别？</a>

<a href="https://mp.weixin.qq.com/s/J3FzYclPQyGl1kofR8xeKQ" target="_blank">21：有哪些创建线程的方法？推荐使用哪种？</a>

<a href="https://mp.weixin.qq.com/s/hKMu4w_PXJ0mYA4FicKkrQ" target="_blank">22：为什么start方法不能重复调用？而run方法却可以？</a>

<a href="https://mp.weixin.qq.com/s/-Y3HBjeVFOBXlOj4qYiakQ" target="_blank">23：说一下线程生命周期，以及转换过程？</a>

<a href="https://mp.weixin.qq.com/s/BWyVLCMq5D98sq8vWa_J9A" target="_blank">24：为什么wait和notify必须放在synchronized中？</a>

<a href="https://mp.weixin.qq.com/s/VwnSAPkvfKa1KnUBNmF7Cg" target="_blank">25：sleep方法和wait方法有什么区别？</a>

<a href="https://mp.weixin.qq.com/s/yCn5knZmRIHpKc1oGes9lg" target="_blank">26：如何正确停止线程？</a>

<a href="https://mp.weixin.qq.com/s/UO9GYxAgc3yarY9Fieqm5Q" target="_blank">27：为什么需要线程池？什么是池化技术？</a>

<a href="https://mp.weixin.qq.com/s/OBBqUUx6MQsPXWUjIPoRAw" target="_blank">28：线程池有几种创建方式？推荐使用哪种？</a>

<a href="https://mp.weixin.qq.com/s/Bd74qpEs20xdRHHN2dWz3A" target="_blank">29：说一下线程池7个参数的含义？</a>

<a href="https://mp.weixin.qq.com/s/hwvEmocK4dqg6Ww7pFzV3w" target="_blank">30：线程池是如何执行的？拒绝策略有哪些？</a>

<a href="https://mp.weixin.qq.com/s/7wqa3Q7BVgSRja1bSO5SJQ" target="_blank">31：什么是守护线程？它和用户线程有什么区别？</a>

<hr>


![](img/gongzhonghao.jpg)

## 208 面试题解析

19 个模块，分别是： **Java 基础、容器、多线程、反射、对象拷贝、Java Web 模块、异常、网络、设计模式、Spring/Spring MVC、Spring Boot/Spring Cloud、Hibernate、Mybatis、RabbitMQ、Kafka、Zookeeper、MySql、Redis、JVM** ，如下图所示：

![](img/java-intervier-gitchat-path.png)


## 目录

[Java 基础部分面试题](doc/200%2B/200_1.md)

[容器部分面试题](doc/200%2B/200_1.md)

[多线程部分面试题](doc/200%2B/200_1.md)

[反射部分面试题](doc/200%2B/200_1.md)

[对象拷贝部分面试题](doc/200%2B/200_2.md)

[Java Web 模块部分面试题](doc/200%2B/200_2.md)

[异常部分面试题网络](doc/200%2B/200_2.md)

[设计模式部分面试题](doc/200%2B/200_3.md)

[Spring/Spring MVC部分面试题](doc/200%2B/200_3.md)

[Spring Boot/Spring Cloud部分面试题](doc/200%2B/200_3.md)

[Hibernate部分面试题](doc/200%2B/200_3.md)

[Mybatis部分面试题](doc/200%2B/200_3.md)

[RabbitMQ部分面试题](doc/200%2B/200_4.md)

[Kafka部分面试题](doc/200%2B/200_4.md)

[Zookeeper部分面试题](doc/200%2B/200_4.md)

[MySql部分面试题](doc/200%2B/200_4.md)

[Redis部分面试题](doc/200%2B/200_4.md)

[JVM部分面试题](doc/200%2B/200_4.md)

## 介绍

此开源项目以“Java 面试题”为切入点，为程序员提供整个职业生涯的服务。这个项目前期的重点是收集和整理一些高质量的面试题，为程序员的职业发展奉献一点力量，更希望聚集更多和我们志趣相投的朋友，来为更多的程序员提供服务。

### 为什么要做这个开源项目？

学的好，不如面的好。

掌握技能和经验固然重要，但面试更重要，因为它才是涨薪的关键。拥有高超的技术 + 超强的面试能力 = 符合自己的高薪工作，而本项目要解决的就是后者。让我们一起卷起来，为了理想中的好工作。

### 投稿

众人拾材火焰高，我们渴望更多的人参与进来，为了帮助更多的人找到理想的工作而奉献自己的一份力量。

**投稿方式**：只需要在本项目下提交 ISsues 进行投稿，为了鼓励大家的踊跃参加，您提交的投稿一经采纳，我们会奖励给投稿人 50 元的购书基金，直接发放现金到您的微信。

